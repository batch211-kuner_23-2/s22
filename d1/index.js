console.log("Hello World");

//Array Methods
// JS has built-in functions and methods for arrays. This allow us to manipulate and access array items.


// push(); -- mag add sa last array
let fruits = ['Apple','Orange','Kiwi','Dragon Fruit'];

console.log('Current Array: ');
console.log(fruits); // print or log the fruits

let fruitsLength = fruits.push('Mango'); // 5 length of array
console.log(fruitsLength);
console.log('Mutated array from push method: ');
console.log(fruits); //mango is added at the end of array

fruits.push('Avocado','Guava');
console.log('Mutated array from push method: ');
console.log(fruits); // nadagdag c avocado at guava

// pop(); --- magremove ng last element sa array

let removeFruit = fruits.pop();
console.log(removeFruit); // guava
console.log('Mutated array from pop method: ');
console.log(fruits); // naremove na c guava..


// Mini Activity
let ghostFighters = ['Eugine','Dennis','Alfred','Taguro'];

function unfriend() {
	ghostFighters.pop();
}
console.log(ghostFighters);


// unshift(); -- magdagdag ng elements pinakaunang array
fruits.unshift('Lime','Banana');
console.log('Mutated array from unshift method: ');
console.log(fruits);


// shift(); --- magremove ng sa pinakaunang element sa array
let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log('Mutated array from shift method: ');
console.log(fruits);


// splice(startingIndex,deleteCount,elementsToBeAdded);

fruits.splice(1,2,'Lime','Cherry'); 
console.log('Mutated array from splice method: ');
console.log(fruits); // mgdelete at magreplace ng laman ng array


//sort();
fruits.sort();
console.log('Mutated array from sort method: ');
console.log(fruits); // alphabetical arrange ang nasa array


// reverse();
fruits.reverse();
console.log('Mutated array from reverse method: ');
console.log(fruits); // binaliktad ang nasa loob ng array


// Non-Mutator Methods --- indexOf() /

let countries = ['US','PH','CAN','SG','TH','PH','FR','DE'];

// indexOf(); -- returns the index number of the first matching element 

let firstIndex = countries.indexOf('PH');
console.log('Result of indexOf method: ' + firstIndex); //index 1

let invalidCountry = countries.indexOf('BR');
console.log('Result of indexOf method: ' + invalidCountry); // index -1

// lastIndexOf(); -- returns the index number of the last matching element 

let lastIndex = countries.lastIndexOf('PH');
console.log('Result of lastIndexOf method: ' + lastIndex);


//slice(); -- hahatiin ang array at mgreturn ng new array

let slicedArrayA = countries.slice(2);
console.log('Result from slice method: ');
console.log(slicedArrayA);

let slicedArrayB = countries.slice(2,4);
console.log('Result from slice method: ');
console.log(slicedArrayB);

let slicedArrayC = countries.slice(-4); // print end sa array backward
console.log('Result from slice method: ');
console.log(slicedArrayC);


// toString(); -- returns array as a string separated by commas

let stringArray = countries.toString();
console.log('Result from toString method: ');
console.log(stringArray);


//concat(); -- combines two arrays and return the combined result or merging

let taskArrayA = ['drink html', 'eat javascript'];
let taskArrayB = ['inhale css', 'breathe sass'];
let taskArrayC = ['get git', 'be node'];

let task = taskArrayA.concat(taskArrayB);
console.log('Result from concat method: ');
console.log(task);

//combining multiple array
console.log('Result from concat method: ');
let allTask = taskArrayA.concat(taskArrayB,taskArrayC);
console.log(allTask);

//combining array with elements
let combineTask = taskArrayA.concat('small express','throw react');
console.log('Result from concat method: ');
console.log(combineTask);


// join(); -- returns an array separated by specified separator string

let users = ['John','Jane','Joe','Robert','Nej'];
console.log(users.join());
console.log(users.join(''));
console.log(users.join(' - '));

//Iteration Method -- are loops designed to perform repetitive task on arrays.

// forEach() -- similar for for loop that iterates on each array element.

allTask.forEach(function(task){
	console.log(task);
});

// Mini Activity

function displayGhostFighters(){
ghostFighters.forEach(function(name){
	console.log(name);
	});
}
displayGhostFighters();


// Using forEach with conditional statements

let filteredTasks = [];
console.log(task);
allTask.forEach(function(task){
	if(task.length>10){
		console.log(task);
		filteredTasks.push(task);
	}
});
console.log('Result of filtered task: ');
console.log(filteredTasks);


// map(); -- iterates on each elements and returns a new array with different values depending on the result of the functions operator.


let numbers =[1,2,3,4,5];

let numberMap = numbers.map(function(number){
	return number * number;
})
console.log('Original Array: ');
console.log(numbers);
console.log('Result of map method: ');
console.log(numberMap);

// map() vs forEach()

let numberForEach = numbers.forEach(function(number){
	return number * number;
});
console.log(numberForEach);


// every(); -- check if all elements in an array meet the given conditions. true or false

let allValid = numbers.every(function(number){
	return (number < 3);
})
console.log('Return of every method: ');
console.log(allValid); // false


// some() -- check if at least one element in the array meets the given condition.

let someValid = numbers.some(function(number){
	return (number<2);
})
console.log('Result of some method: ');
console.log(someValid); // true

// combining the returned result from every/some method may be used in other statements.
if(someValid){
	console.log('Some numbers in the array are greater than 2')
}


// filter() -- returns a new array that contains elements which meets a given condition.

let filterValid = numbers.filter(function(number){
	return (number<3);
}) 
console.log('Result of filter method: ');
console.log(filterValid); // 1,2

let nothingFound = numbers.filter(function(number){
	return (number = 0);
})
console.log('Result of filter method: ');
console.log(nothingFound);


// filtering using forEach

let filteredNumbers = [];

numbers.forEach(function(number){
	// console.log(number);

	if(number<3){
		filteredNumbers.push(number);
	}
})
console.log('Result of filter method: ');
console.log(filteredNumbers);


// includes() -- checks if the arguments passed can be found in the array. it returns booleans

let products = ['Mouse','Keyboard','Laptop','Monitor'];

let productFound = products.includes('Mouse');
console.log(productFound); //true

let productNotFound = products.includes('Headset');
console.log(productNotFound); //false


// Method Chaining -- methods can be chained using them one after another.

let filteredProducts = products.filter(function(product){
	return product.toLowerCase().includes('a');
});
console.log(filteredProducts); // keyboard, laptop


// Mini activity
// revealed answer
let contacts = ['Ash'];

function addTrainer(trainer){
	let doesTrainer = contacts.includes(trainer);

	if(doesTrainerExist){
		alert('Already added in the Match Call.');
	} else {
		contacts.push(trainer);
		alert('Registered!');
	}

}

// reduce() -- evaluates elements from the  left to right and returs/reduces the array into a single value.

console.log(numbers);

let iteration = 0;
let iterationStr = 0;

let reduceArray = numbers.reduce(function(x,y){
	console.warn('Current iteration: ' + ++iteration);
	console.log('accumulator: ' + x);
	console.log('Current value: ' + y);

	return x + y;
})
console.log('Result of reduce method: ' + reduceArray); // 15


let list = ['Hello','Again','World'];

let reducedJoin = list.reduce(function(x,y){
	console.warn('Current iteration: ' + ++iteration);
	console.log('accumulator: ' + x);
	console.log('Current value: ' + y);

	return x + ' ' + y;
})
console.log('Result of reduce method: ' + reducedJoin);